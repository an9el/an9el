---
title: "Trayectoria 2023"
title-block-banner: true
title-slide-attributes:
  data-background-image: ./images/fondo_twas.jpeg
  data-background-size: cover
  data-background-opacity: "0.1"
author: Angel Martinez-Perez
author-meta: Angel Martinez-Perez
institute: Institut de Recerca Sant Pau (IR SANT PAU)
date: 'January 29, 2024'
format:
  revealjs:
    theme: default
    slide-number: c/t
    show-slide-number: speaker
    preview-links: true
    embed-resources: true
    slide-tone: false
    self-contained: false
    auto-stretch: true
    transition: fade
    scrollable: true
    menu: true
    transition-speed: fast
    background-transition: fade
    logo: ./images/IR_campus_pos.png
    footer: "[https://an9el.gitlab.io/an9el](https://an9el.gitlab.io/an9el){preview-link='true'}"
    center: true
    css: estilo.css
tbl-cap-location: bottom
displayMode: compact
from: markdown+emoji
---

## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}

    
```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B-%d

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
```
::: {.notes}

1. Era un projecto en colaboración con:
   * David Tregouet
   * Marta Sanz
   * Blandine Gendre, para su tesis
   
Se trataba de buscar efectos de Parent of Origin con datos no relacionados con los niveles de *FV* y su interacción con el SNP, rs75463553.
Para ello utilizamos los datos del RETROVE, que un estudio prospectivo con 400 trombosis venosa y 400 controles, macheados por sexo y siguiendo la distribución de la pirámide poblacinal española.

El método utilizado era bastante novedoso, porque el Parent of Origin Effect, que es un extraño efecto observado en genética en el que vemos que tener alelo tiene produce diferente expresión génica dependiendo de si ese alelo lo has heredado de tu padre o de tu madre. Observemos lo extraño que es eso para alguien que es matemático, estoy diciendo que la un gen con una mutación funciona diferente si esa mutación la has heredado de tu padre, que ese mismo gen con la misma mutación, pero esta vez heredada de tu madre.
:::


## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B-%d

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
```


::: {.notes}

En el consorcium INVENT lo que hicimos fue analizar en `RETROVE` y `gait2` determinantes genéticas que estuviesen
implicadas en el desarrollo de las complicaciones a embolia de pulmón en pacientes que tuviesen trombosis venosa profunda.
Para ello partimos la muestra en tres tipos (`DVT alone`, `DVT+PE`, `PE alone`) y de otro fenotipo dicotimizando
`DVT alone` y `PE with or without DVT`. 
Utilizamos un nuevo método, *proportional odds logistic mixed model* (POLMM), que es capaz de encontrar asociación
con variantes raras y muy raras, generalizando métodos de colapsamiento como SKAT o SKAT-O, para fenotipos dicotómicos.

:::


## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
```


::: {.notes}

HL154385 INVENT-TOPMed SNV and SV meta-analyses. Se realizó tanto en el `gait2` como en retrove.
En el gait2 se tuvo que recortar la muestra para que como máximo hubiese 4 controles por cada caso.
Ha sido enviado a `circulation` y está camino de ser publicado

:::

## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
```


::: {.notes}

Realizamos el análisis en el `RETROVE` de los niveles de la proteina `CFHR5`, como reguladora de la vía
de la coagulación del complemento. 
Miramos también proteómica e interacción SNP-fenotipo.
Es una colaboración con David Tregouet que replicó sus resultados en
una muestra del Karolinska y en el RETROVE. Finalmente se acabó publicando en `Nature communications`.


:::



## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-04-28
```


::: {.notes}

Se realizó el testing en el gait1, donde tenemos familias, del estudio de Parent of Origin
donde pudimos saber cuales on los alelos heredados del padre y de la madre y miramos si
existían diferencias. Lamentablemente el el `gait1` no se veían diferencias puesto que había
muy poca muestra, logramos ver alguna interacción con el tabaco al menos.

:::





## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
```


::: {.notes}

Análisis de GWAS de las proteínas en el retrove 

:::



## 2023  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	FIX           :done, FIX, 2023-09-14,2023-12-20
```


::: {.notes}

Análisis de GWAS de fenotipos para el charge, en este caso son fenotipos relacionados
con la hemostasis y se analizó el *FIX* 

:::

## an9elproject  {transition="convex" auto-animate=false}

![](images/2023_versions.png){fig-align="center"}

## 

![](images/2023_habitacion.jpeg){fig-align="center"}

##  {transition="convex" auto-animate=false}

![](images/2023_n_versions.png){fig-align="center"}

##  {transition="convex" auto-animate=false}

![](images/2023_get_project.png){fig-align="center"}


##  {transition="convex" auto-animate=false}

![](images/2023_g2_old.png){fig-align="center"}

. . .

![](images/2023_gait2.png){fig-align="center"}


##  {transition="convex" auto-animate=false}

![](images/2023_tabs.png){fig-align="center"}


## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Activos
	structural    :active, structural, 2022-11-16,2024-01-29
```


::: {.notes}

Este puede ser el projecto al que le he dedicado más tiempo en 2023.
Se trata de sacar las variantes estructurales tanto en el gait2 como en retrove.
Las variantes structurales, como su nombre indica son variantes relativas a la estructura
del ADN, detectaríamos inversiones, translocaciones, aberraciones comosómicas, grandes delecciones.

Este es un análisis gigante realizado con muchos conocimientos biológicos e informáticos.

Se parte directamente de los datos más crudos de la genotipación, de los datos de intensidades y desde allí
se reconstruye los datos hasta buscar lo que queremos. Luego se tratará de asociar lo encontrado, 
con el riesgo de trombosis.

:::



## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Activos
	structural    :active, structural, 2022-11-16,2024-01-29
	tabs          :active, crecimiento aortic diameter, 2023-06-22,2024-01-29
```


::: {.notes}


En el projecto `tabs` se van tomando mediciones del diámetro de la aorta de pacientes con
riesgo de sufrir una rotura con la aorta, y nuestro objetivo es caracterizar matemáticamente
este crecimiento. En particular estamos muy interesados en modelizar la velocidad, para así
además de poder predecir el punto óptimo para realizar la operación porque no hacerlo conllevaría
un riesgo para el paciente inasumible, también podemos optimizar la citación del paciente
para su seguimiento evitando visitas al doctor inecesarias, con lo que tendríamos un doble
beneficio, para el paciente y para el sistema de salud-doctores.

:::


## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Para el 2024
	structural    :active, structural, 2022-11-16,2024-01-29
	tabs          :active, crecimiento aortic diameter, 2023-06-22,2024-01-29
	TWAS-EA2      :active, TWAS dvt-pe, 2023-07-03,2024-01-20
```


::: {.notes}

Se trata aquí de realizar el transcriptome wide association analisis para identificar
si la expresión de algunos genes en diferentes tejidos nos permite distinguir aquellos
pacientes que tendrán una trobosis profunda de aquellos que desarrollarán también
una embolia de pulmón. No se utilizó muestra de Sant Pau, sino que es una muestra externa
y hemos recibido sólo los summary statistics.


:::


## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Para el 2024
	structural    :active, structural, 2022-11-16,2024-01-29
	TWAS-EA2      :active, TWAS dvt-pe, 2023-07-03,2024-01-20
	tabs          :active, crecimiento aortic diameter, 2023-06-22,2024-01-29
	aging         :active, antiaging score, 2024-01-10,2024-03-01
```


::: {.notes}

Con la muestra del gait2 se trata replicar scores validados de edad fenotípica, y con esa
medición hacer una selección de variables o creación de variables de expresión que nos prediga
esa edad fenotípica y enriquecerla en vías de forma que puedan ser accionables.

:::



## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Para el 2024
	structural    :active, structural, 2022-11-16,2024-01-29
	TWAS-EA2      :active, TWAS dvt-pe, 2023-07-03,2024-01-20
	tabs          :active, crecimiento aortic diameter, 2023-06-22,2024-01-29
	aging         :active, antiaging score, 2024-01-10,2024-03-01
	lipids        :active, Lipids consortium, 2024-01-30,2024-03-08
```


::: {.notes}

Con la muestra del global lpids genetics consortium (Muestra del procardis y polca)  relizaremos unos
análisis para ver si existe interacción entre el BMI y la edad junto con perfil lipídico.
Todavía estoy en la fase de poder acceder a los datos y empezar el análisis.

:::



## 2024  {transition="convex" auto-animate=true auto-animate-easing="ease-in-out"}


```{mermaid}
gantt
    dateFormat  YYYY-MM-DD
	axisFormat %Y-%B

    section Durante 2023
    POE           :active, poe, 2022-03-28,2023-11-20
	dvt-pe        :active, dvt-pe, 2022-04-22,2023-04-28
	HL154385      :done, HL154385, 2022-05-17,2023-01-01
	CFHR5         :done, CFHR5, 2022-05-17,2023-04-28
	poe-rs7070780 :done, rs7070780, 2022-09-21,2023-03-18
	proteomics    :done, proteomics RETROVE, 2022-10-04,2023-05-12
	
    section Para el 2024
	structural    :active, structural, 2022-11-16,2024-01-29
	TWAS-EA2      :active, TWAS dvt-pe, 2023-07-03,2024-01-20
	tabs          :active, crecimiento aortic diameter, 2023-06-22,2024-01-29
	aging         :active, antiaging score, 2024-01-10,2024-03-01
	lipids        :active, Lipids consortium, 2024-01-30,2024-03-08
	gait2         :active, gait2 lessons, 2024-03-09,2024-12-31
```


::: {.notes}

Realizar un paper compendio de resultados del gait2.

:::

