---
title: "docker"
subtitle: "installation and examples"
title-block-banner: true
author: 
  - name: Angel Martinez-Perez
    orcid: 0000-0002-5934-1454
    affiliations:
      - name: Genomics of Complex Diseases Group. IIB Sant Pau
        address: Carrer de Sant Quintí, 77
        city: Barcelona
        state: Spain
        postal-code: 08041
date: 2022-11-11
date-format: iso
tbl-cap-location: bottom
execute:
  echo: false
  warning: false
---

## Install docker

From the official [web](https://docs.docker.com/engine/install/debian/#install-using-the-repository/)

:::{.callout-note collapse="true"}
# set up the repository
* `sudo apt-get update`
* `sudo apt-get install ca-certificates curl gnupg lsb-release`

### Add Docker’s official GPG key
* `sudo mkdir -p /etc/apt/keyrings`
* `curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg`

### Set up the repository
* `echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`

### Install docker
* `sudo chmod a+r /etc/apt/keyrings/docker.gpg`
* `sudo apt-get update`
* `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin`

### To run docker rootless, disable the system wide daemon
* `sudo systemctl disable --now docker.service docker.socket`
:::

## Docker rootless

See the [official documentation](https://docs.docker.com/engine/security/rootless/)

### Prerequisites in `debian`

* `sudo apt-get install -y dbus-user-session`
* `sudo apt-get install -y fuse-overlayfs`
* `sudo apt-get install -y slirp4netns`
* `sudo apt-get install -y docker-ce-rootless-extras`
* `sudo apt-get install -y uidmap`

### Install docker rootless as non-root user:

* `dockerd-rootless-setuptool.sh install`

If docker service is masked,
(you can check it with: `sudo systemctl list-unit-files` or `systemctl status docker.service`)
we unmask it with: 
`sudo systemctl unmask docker`

## Uninstall docker

:::{.callout-note collapse="true"}
## Identify which docker packages have been installed
* `dpkg -l | grep -i docker`

### Remove the packages
* `sudo apt-get purge -y docker-ce docker-ce-cli`

### Remove also the docker related files
* sudo rm -rf /var/lib/docker /etc/docker
* sudo rm /etc/apparmor.d/docker
* sudo groupdel docker
* sudo rm -rf /var/run/docker.sock

### Deactivate network interface

* `ifconfig docker0 down`
* `brctl delbr docker0`
:::


## install podman

:::{.callout-note collapse="true"}
## debian 11

Thats don't work to install the latest version on debian.
The version `3.0.1` is in the repositories, but gives problems with `rocker/tidyverse`

```
sudo apt-get install \
  btrfs-progs \
  crun \
  git \
  golang-go \
  go-md2man \
  iptables \
  libassuan-dev \
  libbtrfs-dev \
  libc6-dev \
  libdevmapper-dev \
  libglib2.0-dev \
  libgpgme-dev \
  libgpg-error-dev \
  libprotobuf-dev \
  libprotobuf-c-dev \
  libseccomp-dev \
  libselinux1-dev \
  libsystemd-dev \
  pkg-config \
  uidmap \
  libapparmor-dev
```
  
* `sudo apt install podman`
* `podman --version`
* `podman info`

### configure podman registries

* `sudo vim /etc/containers/registries.conf`

and add the following line

`unqualified-search-registries = [ 'registry.access.redhat.com', 'registry.redhat.io', 'docker.io']`

### pull images

To pull or push images first we need to login the platform, for example `docker.io`:

* `podman login docker.io`
* `podman search r-base`
* `podman pull docker.io/rocker/tidyverse:4.2.2`
* `podman logout  docker.io`

### volumes

This volume work because is interactive. We are able to write in the volume.

* `podman run --name amartinezp --rm -it -v /home/amartinezp/kk:/home/angel:Z docker.io/rocker/tidyverse:4.2.2 R`

### ports

This volume don't work don't know why. It is the same as before but now it launch `rstudio`:

* `podman run --name amartinezp --rm -it -p 8787:8787 -e PASSWORD="tit" -v /home/amartinezp/kk:/home/rstudio:Z docker.io/rocker/tidyverse:4.2.2`

### Comandos que no funcionan

* `podman run -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2` arranca pero no puede grabar `system('id') uid=1000(rstudio) gid=1000(rstudio) groups=1000(rstudio),27(sudo),50(staff)`


### Comandos que SI funcionan
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`


### Esto me lo hace bien pero me cambia los permisos
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e USER=amartinezp -e USERID=1001 -e GROUPID=1001 -e ROOT=TRUE docker.io/rocker/tidyverse:4.2.2`

### With Sergui
* `podman run --name amartinezp -p 8787:8787 --rm -it docker.io/rocker/tidyverse:4.2.2 bash`
* `podman run --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`

## Este funciona cuando el usuario es 1000

* `podman run --userns=keep-id -u root --name amartinezp -p 8787:8787 -e USERID=1000 -e GROUPID=1000 --rm -it -v /home/amartinezp/test:/home/rstudio docker.io/rocker/tidyverse:4.2.2`
* Incluso no hace falta poner las variables de entorno `USERID` ni `GROUPID`
:::
